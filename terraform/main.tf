terraform {
    required_version = ">= 0.12.9"
}

terraform {
    backend "s3" {
        bucket = "oregon-tfstates"
        key    = "react-site.tfstates"
        region = "us-west-2"
    }
}

#IAM User gitlab-deployer
provider "aws" {
   version = "= 3.2.0"   
}

# s3 bucket
resource "aws_s3_bucket" "site_bucket" {
  arn            = "arn:aws:s3:::${var.bucket_name}"
  bucket         = var.bucket_name
  force_destroy  = "true"
  request_payer  = "BucketOwner"

  versioning {
    enabled    = "false"
    mfa_delete = "false"
  }

  website {
    error_document = "index.html"
    index_document = "index.html"
  }

  website_domain   = "s3-website-us-west-2.amazonaws.com"
  website_endpoint = "${var.bucket_name}.s3-website-us-west-2.amazonaws.com"
}
